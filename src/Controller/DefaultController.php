<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/address")
     */
    public function addressAction()
    {
        $request = [
	        "AccessRequest" => [
		        "AccessLicenseNumber" => "4D1A29D975F9D428",
		        "UserId" => "shipreliant",
		        "Password" => "Password5656$"
	        ],
	        "AddressValidationRequest" => [ 
		        "Request" => [
			        "TransactionReference" => [
				        "CustomerContext" => "Your Customer Context"
			        ],
			        "RequestAction" => "AV"
		        ],
		        "Address" => [
			        "City" => "ALPHARETTA",
			        "StateProvinceCode" => "GA",
			        "PostalCode" => "30005"
		        ]
	        ]
        ];
        $restClient = $this->container->get('circle.restclient');
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_address_url'), 
            json_encode($request)
        )->getContent();
        die;
    }

    /**
     * @Route("/street")
     */
    public function streetAction()
    {
        $restClient = $this->container->get('circle.restclient');
        $request = [
            "UPSSecurity"=> [
                "UsernameToken"=> [
                    "Username"=> $this->container->getParameter('ups_login'),
                    "Password"=> $this->container->getParameter('ups_password')
                ],
                "ServiceAccessToken"=> [
                    "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
                ]
            ],
            "XAVRequest"=> [
                "Request"=> [
                    "RequestOption"=> "1",
                    "TransactionReference"=> [
                        "CustomerContext"=> "Your Customer Context"
                    ]
                ],
                "MaximumListSize"=> "10",
                "AddressKeyFormat"=> [
                    "ConsigneeName"=> "Consignee Name",
                    "BuildingName"=> "Building Name",
                    "AddressLine"=> "12380 Morris Road",
                    "PoliticalDivision2"=> "Alpharetta",
                    "PoliticalDivision1"=> "GA",
                    "PostcodePrimaryLow"=> "30009",
                    "CountryCode"=> "US"
                ]
            ]
        ];
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_street_url'), 
            json_encode($request)
        )->getContent();
        die;
    }

    /**
     * @Route("/shipment")
     */
    public function shipmentAction()
    {
$request = [
    "UPSSecurity"=> [
        "UsernameToken"=> [
            "Username"=> $this->container->getParameter('ups_login'),
            "Password"=> $this->container->getParameter('ups_password')
        ],
        "ServiceAccessToken"=> [
            "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
        ]
    ],
    "ShipmentRequest"=> [
        "Request"=> [
            "RequestOption"=> "validate",
            "TransactionReference"=> [
                "CustomerContext"=> "Your Customer Context"
            ]
        ],
        "Shipment"=> [
            "Description"=> "Description",
            "Shipper"=> [
                "Name"=> "Shipper Name",
                "AttentionName"=> "Shipper Attn Name",
                "TaxIdentificationNumber"=> "123456",
                "Phone"=> [
                    "Number"=> "1234567890",
                    "Extension"=> "1"
                ],
                "ShipperNumber"=> $this->container->getParameter('ups_shipper'),
                "FaxNumber"=> "1234567890",
                "Address"=> [
                    //"AddressLine"=> "118 2ND ST",
                    //"City"=> "San Francisco",
                    //"StateProvinceCode"=> "CA",
                    //"PostalCode"=> "12345",
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "ShipTo"=> [
                "Name"=> "Ship To Name",
                "AttentionName"=> "118 2ND ST",
                "Phone"=> [
                    "Number"=> "1234567890"
                ],
                "Address"=> [
                    //"AddressLine"=> "118 2ND ST",
                    //"City"=> "San Francisco",
                    //"StateProvinceCode"=> "CA",
                    //"PostalCode"=> "12345",
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "ShipFrom"=> [
                "Name"=> "Ship From Name",
                "AttentionName"=> "Ship From Attn Name",
                "Phone"=> [
                    "Number"=> "1234567890"
                ],
                "FaxNumber"=> "1234567890",
                "Address"=> [
                    //"AddressLine"=> "118 2ND ST",
                    //"City"=> "San Francisco",
                    //"StateProvinceCode"=> "CA",
                    //"PostalCode"=> "12345",
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "PaymentInformation"=> [
                "ShipmentCharge"=> [
                    "Type"=> "01",
                    "BillShipper"=> [
                        "AccountNumber"=> $this->container->getParameter('ups_shipper')
                    ]
                ]
            ],
            "Service"=> [
                "Code"=> "01",
                "Description"=> "Express"
            ],
            "Package"=> [
                "Description"=> "Description",
                "Packaging"=> [
                    "Code"=> "02",
                    "Description"=> "Description"
                ],
                "Dimensions"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "IN",
                        "Description"=> "Inches"
                    ],
                    "Length"=> "7",
                    "Width"=> "5",
                    "Height"=> "2"
                ],
                "PackageWeight"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "LBS",
                        "Description"=> "Pounds"
                    ],
                    "Weight"=> "10"
                ]
            ]
        ],
        "LabelSpecification"=> [
            "LabelImageFormat"=> [
                "Code"=> "GIF",
                "Description"=> "GIF"
            ],
            "HTTPUserAgent"=> "Mozilla/4.5"
        ]
    ]
];
        $restClient = $this->container->get('circle.restclient');
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_shipment_url'), 
            json_encode($request)
        )->getContent();
        die;
    }

    /**
     * @Route("/rate")
     */
    public function rateAction()
    {
$request = [
    "UPSSecurity"=> [
        "UsernameToken"=> [
            "Username"=> $this->container->getParameter('ups_login'),
            "Password"=> $this->container->getParameter('ups_password')
        ],
        "ServiceAccessToken"=> [
            "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
        ]
    ],
    "RateRequest"=> [
        "Request"=> [
            "RequestOption"=> "Rate",
            "TransactionReference"=> [
                "CustomerContext"=> "Your Customer Context"
            ]
        ],
        "Shipment"=> [
            "Shipper"=> [
                "Name"=> "Shipper Name",
                "ShipperNumber"=> $this->container->getParameter('ups_shipper'),
                "Address"=> [
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "ShipTo"=> [
                "Name"=> "Ship To Name",
                "Address"=> [
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "ShipFrom"=> [
                "Name"=> "Ship From Name",
                "Address"=> [
                    "AddressLine" => "1012 N turner 242",
                    "CountryCode"=> "US",
			        "City" => "ONTARIO",
			        "StateProvinceCode" => "CA",
			        "PostalCode" => "91764",
                ]
            ],
            "Service"=> [
                "Code"=> "01",
                "Description"=> "Service Code Description"
            ],
            "Package"=> [
                "PackagingType"=> [
                    "Code"=> "02",
                    "Description"=> "Rate"
                ],
                "Dimensions"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "IN",
                        "Description"=> "inches"
                    ],
                    "Length"=> "5",
                    "Width"=> "4",
                    "Height"=> "3"
                ],
                "PackageWeight"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "Lbs",
                        "Description"=> "pounds"
                    ],
                    "Weight"=> "1"
                ]
            ],
            "ShipmentRatingOptions"=> [
                "NegotiatedRatesIndicator"=> ""
            ]
        ]
    ]
];
        $restClient = $this->container->get('circle.restclient');
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_rate_url'), 
            json_encode($request)
        )->getContent();
        die;

    }

    /**
     * @Route("/pickup")
     */
    public function pickupAction()
    {
$request = [
    "UPSSecurity"=> [
        "UsernameToken"=> [
            "Username"=> $this->container->getParameter('ups_login'),
            "Password"=> $this->container->getParameter('ups_password')
        ],
        "ServiceAccessToken"=> [
            "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
        ]
    ],
    "PickupCreationRequest"=> [
        "Request"=> [
            "TransactionReference"=> [
                "CustomerContext"=> "CustomerContext."
            ]
        ],
        "RatePickupIndicator"=> "Y",
        "TaxInformationIndicator"=> "Y",
        "PickupDateInfo"=> [
            "CloseTime"=> "1700",
            "ReadyTime"=> "1000",
            "PickupDate"=> "20160524"
        ],
        "PickupAddress"=> [
            "CompanyName"=> "EIVR TEST NON-ACCOUNT PL 3",
            "ContactName"=> "EIVR TES PL",
            "AddressLine"=> "12380 Morris Rd",
            "City"=> "Louisville",
            "StateProvince"=> "KY",
            "PostalCode"=> "40201",
            "CountryCode"=> "US",
            "ResidentialIndicator"=> "N",
            "Phone"=> [
                "Number"=> "+48223803070"
            ]
        ],
        "AlternateAddressIndicator"=> "N",
        "PickupPiece"=> [
            "ServiceCode"=> "003",
            "Quantity"=> "1",
            "DestinationCountryCode"=> "US",
            "ContainerCode"=> "01"
        ],
        "OverweightIndicator"=> "N",
        "PaymentMethod"=> "01"
    ]
];
        $restClient = $this->container->get('circle.restclient');
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_pickup_url'), 
            json_encode($request)
        )->getContent();
        die;
    }
}
