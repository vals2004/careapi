<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;

class UserController extends FOSRestController
{
    /**
     * @Rest\Get("/user")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:User')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/user/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('ApiBundle:User')->find($id);
        if ($singleresult === null) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }    

    /**
    * @Rest\Post("/user/")
    */
    public function postAction(Request $request)
    {
        try {
            $data = new User;
            $email = $request->get('email');
            $password = $request->get('password');
            $role = $request->get('role');
            //if(empty($name) || empty($role)) {
                //return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
            //} 
            $data->setEmail($email);
            $data->setPassword($password);
            $data->setRole($role);
            $data->setFirstName($request->get('first_name'));
            $data->setLastName($request->get('last_name'));
            $data->setPhone($request->get('phone'));
            $data->setBalance($request->get('balance'));
            
            $validator = $this->get('validator');
            $errors = $validator->validate($data);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            return $data;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }

    /**
     * @Rest\Put("/user/{id}")
     */
    public function updateAction($id,Request $request)
    { 
        $data = new User;
        $email = $request->get('email');
        $password = $request->get('password');
        $role = $request->get('role');
        $sn = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($id);
        if (empty($user)) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        } 
        elseif(!empty($email) && !empty($role) && !empty($password)) {
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setRole($role);
            $sn->flush();
            return new View("User Updated Successfully", Response::HTTP_OK);
        }
        /*elseif(empty($name) && !empty($role)) {
            $user->setRole($role);
            $sn->flush();
            return new View("role Updated Successfully", Response::HTTP_OK);
        }
        elseif(!empty($name) && empty($role)) {
            $user->setName($name);
            $sn->flush();
            return new View("User Name Updated Successfully", Response::HTTP_OK); 
        }*/
        else {
            return new View("Fields cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
        }
    }   

    /**
     * @Rest\Delete("/user/{id}")
     */
    public function deleteAction($id)
    {
        $data = new User;
        $sn = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($id);
        if (empty($user)) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($user);
            $sn->flush();
        }
        return new View("deleted successfully", Response::HTTP_OK);
    }    
}

