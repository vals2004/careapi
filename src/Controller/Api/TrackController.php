<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\Shipment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class TrackController extends FOSRestController
{
    /**
     * @Rest\Get("/track/{id}")
     * @ParamConverter("shipment", class="ApiBundle:Shipment")
     */
    public function idAction(Shipment $shipment)
    {
        if ($shipment === null) {
            return new View("shipment not found", Response::HTTP_NOT_FOUND);
        }
        return $shipment;
    }    
}

