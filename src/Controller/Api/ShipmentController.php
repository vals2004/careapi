<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\State;
use App\Entity\Shipment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ShipmentController extends FOSRestController
{
    /**
     * @Rest\Get("/shipment")
     * @ ParamConverter("user", class="ApiBundle:User")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:Shipment')->findAll();
        if ($restresult === null) {
            return new View("there are no shipment exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/shipment/{id}")
     * @ParamConverter("shipment", class="ApiBundle:Shipment")
     */
    public function idAction(Shipment $shipment)
    {
        if ($shipment === null) {
            return new View("shipment not found", Response::HTTP_NOT_FOUND);
        }
        return $shipment;
    }    

    /**
     * @Rest\Put("/shipment/buy")
     */
    public function buyAction(Request $request)
    {
        try {
            $shipment = $this->getDoctrine()->getRepository('ApiBundle:Shipment')->find($request->get('shipment_id'));
            $shipment->setStatus('in process');
            $user = $shipment->user;
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($shipment);
            $em->flush();
            return $shipment;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }

    /**
     * Create new Shipment
     *
     * @Rest\Post("/shipment/")
     */
    public function postAction(Request $request)
    {
        try {
            $shipment = new Shipment;
            $shipment->setStatus('new');
            $shipment->setDate(new \DateTime($request->get('date')));
            $fromAddress = $this->getDoctrine()->getRepository('ApiBundle:Address')->find($request->get('from_address_id'));
            if (!$fromAddress) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $shipment->setFromAddress($fromAddress);
            $toAddress = $this->getDoctrine()->getRepository('ApiBundle:Address')->find($request->get('to_address_id'));
            if (!$toAddress) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $shipment->setToAddress($toAddress);
            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($request->get('user_id'));
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $shipment->setUser($user);
            $parcel = $this->getDoctrine()->getRepository('ApiBundle:Parcel')->find($request->get('parcel_id'));
            if (!$parcel) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $shipment->setParcel($parcel);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($shipment);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }

            $upsResponse = $this->checkUps($shipment);
            if ($upsResponse->RateResponse->Response->ResponseStatus->Code!="1") {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }

            $shipment->setUpsResponse($upsResponse);
            $em = $this->getDoctrine()->getManager();
            $em->persist($shipment);
            $em->flush();
            return $shipment;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }

    private function checkUps($shipment)
    {
$request = [
    "UPSSecurity"=> [
        "UsernameToken"=> [
            "Username"=> $this->container->getParameter('ups_login'),
            "Password"=> $this->container->getParameter('ups_password')
        ],
        "ServiceAccessToken"=> [
            "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
        ]
    ],
    "RateRequest"=> [
        "Request"=> [
            "RequestOption"=> "Rate",
            "TransactionReference"=> [
                "CustomerContext"=> "Your Customer Context"
            ]
        ],
        "Shipment"=> [
            "Shipper"=> [
                "Name"=> "Shipper Name",
                "ShipperNumber"=> $this->container->getParameter('ups_shipper'),
                "Address"=> [
                    "AddressLine" => $shipment->getFromAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getFromAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getFromAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getFromAddress()->getZip(),
                ]
            ],
            "ShipTo"=> [
                "Name"=> "Ship To Name",
                "Address"=> [
                    "AddressLine" => $shipment->getToAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getToAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getToAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getToAddress()->getZip(),
                ]
            ],
            "ShipFrom"=> [
                "Name"=> "Ship From Name",
                "Address"=> [
                    "AddressLine" => $shipment->getFromAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getFromAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getFromAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getFromAddress()->getZip(),
                ]
            ],
            "Service"=> [
                "Code"=> "03",
                "Description"=> "Service Code Description"
            ],
            "Package"=> [
                "PackagingType"=> [
                    "Code"=> "02",
                    "Description"=> "Rate"
                ],
                "Dimensions"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "IN",
                        "Description"=> "inches"
                    ],
                    "Length"=> $shipment->getParcel()->getLength(),
                    "Width"=> $shipment->getParcel()->getWidth(),
                    "Height"=> $shipment->getParcel()->getHeight()
                ],
                "PackageWeight"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "Lbs",
                        "Description"=> "pounds"
                    ],
                    "Weight"=> $shipment->getParcel()->getWeight()
                ]
            ],
            "ShipmentRatingOptions"=> [
                "NegotiatedRatesIndicator"=> ""
            ]
        ]
    ]
];
        $restClient = $this->container->get('circle.restclient');
        return json_decode($restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_rate_url'), 
            json_encode($request)
        )->getContent());

    }
}

/*public function buyUps($shipment)
    {
$request = [
    "UPSSecurity"=> [
        "UsernameToken"=> [
            "Username"=> $this->container->getParameter('ups_login'),
            "Password"=> $this->container->getParameter('ups_password')
        ],
        "ServiceAccessToken"=> [
            "AccessLicenseNumber"=> $this->container->getParameter('ups_api_key')
        ]
    ],
    "ShipmentRequest"=> [
        "Request"=> [
            "RequestOption"=> "validate",
            "TransactionReference"=> [
                "CustomerContext"=> "Your Customer Context"
            ]
        ],
        "Shipment"=> [
            "Description"=> "Description",
            "Shipper"=> [
                "Name"=> "Shipper Name",
                "AttentionName"=> "Shipper Attn Name",
                "TaxIdentificationNumber"=> "123456",
                "Phone"=> [
                    "Number"=> "1234567890",
                    "Extension"=> "1"
                ],
                "ShipperNumber"=> $this->container->getParameter('ups_shipper'),
                "FaxNumber"=> "1234567890",
                "Address"=> [
                    "AddressLine" => $shipment->getFromAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getFromAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getFromAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getFromAddress()->getZip(),
                ]
            ],
            "ShipTo"=> [
                "Name"=> "Ship To Name",
                "AttentionName"=> "118 2ND ST",
                "Phone"=> [
                    "Number"=> "1234567890"
                ],
                "Address"=> [
                    "AddressLine" => $shipment->getToAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getToAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getToAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getToAddress()->getZip(),
                ]
            ],
            "ShipFrom"=> [
                "Name"=> "Ship From Name",
                "AttentionName"=> "Ship From Attn Name",
                "Phone"=> [
                    "Number"=> "1234567890"
                ],
                "FaxNumber"=> "1234567890",
                "Address"=> [
                    "AddressLine" => $shipment->getFromAddress()->getAddress(),
                    "CountryCode"=> "US",
			        "City" => $shipment->getFromAddress()->getCity(),
			        "StateProvinceCode" => $shipment->getFromAddress()->getState()->getCode(),
			        "PostalCode" => $shipment->getFromAddress()->getZip(),
                ]
            ],
            "PaymentInformation"=> [
                "ShipmentCharge"=> [
                    "Type"=> "01",
                    "BillShipper"=> [
                        "AccountNumber"=> $this->container->getParameter('ups_shipper')
                    ]
                ]
            ],
            "Service"=> [
                "Code"=> "01",
                "Description"=> "Express"
            ],
            "Package"=> [
                "Description"=> "Description",
                "Packaging"=> [
                    "Code"=> "02",
                    "Description"=> "Description"
                ],
                "Dimensions"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "IN",
                        "Description"=> "Inches"
                    ],
                    "Length"=> "7",
                    "Width"=> "5",
                    "Height"=> "2"
                ],
                "PackageWeight"=> [
                    "UnitOfMeasurement"=> [
                        "Code"=> "LBS",
                        "Description"=> "Pounds"
                    ],
                    "Weight"=> "10"
                ]
            ]
        ],
        "LabelSpecification"=> [
            "LabelImageFormat"=> [
                "Code"=> "GIF",
                "Description"=> "GIF"
            ],
            "HTTPUserAgent"=> "Mozilla/4.5"
        ]
    ]
];
        $restClient = $this->container->get('circle.restclient');
        header('Content-Type: application/json');
        echo $restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_shipment_url'), 
            json_encode($request)
        )->getContent();
        die;
    }*/
