<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\State;
use App\Entity\Shipment;
use App\Entity\Pickup;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class PickupController extends FOSRestController
{
    /**
     * @Rest\Get("/pickup")
     * @ ParamConverter("user", class="ApiBundle:User")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:Pickup')->findAll();
        if ($restresult === null) {
            return new View("there are no pickup exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/pickup/{id}")
     * @ParamConverter("pickup", class="ApiBundle:Pickup")
     */
    public function idAction(Pickup $pickup)
    {
        if ($pickup === null) {
            return new View("pickup not found", Response::HTTP_NOT_FOUND);
        }
        return $pickup;
    }    

    /**
    * @Rest\Post("/pickup/")
    */
    public function postAction(Request $request)
    {
        try {
            $pickup = new Pickup;
            $pickup->setDate(new \DateTime($request->get('date')));
            $address = $this->getDoctrine()->getRepository('ApiBundle:Address')->find($request->get('address_id'));
            if (!$address) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $pickup->setAddress($address);
            $shipment = $this->getDoctrine()->getRepository('ApiBundle:Shipment')->find($request->get('shipment_id'));
            if (!$shipment) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $pickup->setShipment($shipment);
            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($request->get('user_id'));
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $pickup->setUser($user);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($pickup);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($pickup);
            $em->flush();
            return $pickup;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }
}

