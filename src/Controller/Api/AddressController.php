<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\State;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AddressController extends FOSRestController
{
    /**
     * @Rest\Get("/address")
     * @ ParamConverter("user", class="ApiBundle:User")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:Address')->findAll();
        if ($restresult === null) {
            return new View("there are no address exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/address/{id}")
     * @ParamConverter("address", class="ApiBundle:Address")
     */
    public function idAction(Address $address)
    {
        if ($address === null) {
            return new View("address not found", Response::HTTP_NOT_FOUND);
        }
        return $address;
    }    

    /**
    * @Rest\Post("/address/")
    */
    public function postAction(Request $request)
    {
        try {
            $address = new Address;
            $address->setAddress($request->get('address'));
            $address->setZip($request->get('zip'));
            $address->setCity($request->get('city'));
            $address->setPhone($request->get('phone'));
            $address->setEmail($request->get('email'));
            $state = $this->getDoctrine()->getRepository('ApiBundle:State')->find($request->get('state_id'));
            if (!$state) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $address->setState($state);
            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($request->get('user_id'));
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $address->setUser($user);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($address);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }

            $upsResponce = $this->checkUps($address);
            if ($upsResponce->AddressValidationResponse->Response->ResponseStatusCode!="1") {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();
            return $address;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }

    private function checkUps($address)
    {
        $request = [
	        "AccessRequest" => [
		        "AccessLicenseNumber" => $this->container->getParameter('ups_api_key'),
		        "UserId" => $this->container->getParameter('ups_login'),
		        "Password" => $this->container->getParameter('ups_password')
	        ],
	        "AddressValidationRequest" => [ 
		        "Request" => [
			        "TransactionReference" => [
				        "CustomerContext" => "Your Customer Context"
			        ],
			        "RequestAction" => "AV"
		        ],
		        "Address" => [
			        "City" => $address->getCity(),
			        "StateProvinceCode" => $address->getState()->getCode(),
			        "PostalCode" => $address->getZip(),
		        ]
	        ]
        ];
        $restClient = $this->container->get('circle.restclient');
        return json_decode($restClient->post(
            $this->container->getParameter('ups_base_url').$this->container->getParameter('ups_address_url'), 
            json_encode($request)
        )->getContent());

    }
}

