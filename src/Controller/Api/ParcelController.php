<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;
use App\Entity\Parcel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ParcelController extends FOSRestController
{
    /**
     * @Rest\Get("/parcel")
     * @ ParamConverter("user", class="ApiBundle:User")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:Parcel')->findAll();
        if ($restresult === null) {
            return new View("there are no parcel exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/parcel/{id}")
     * @ParamConverter("parcel", class="ApiBundle:Parcel")
     */
    public function idAction(Parcel $parcel)
    {
        if ($parcel === null) {
            return new View("parcel not found", Response::HTTP_NOT_FOUND);
        }
        return $parcel;
    }    

    /**
    * @Rest\Post("/parcel/")
    */
    public function postAction(Request $request)
    {
        try {
            $parcel = new Parcel;
            $parcel->setName($request->get('name'));
            $parcel->setLength($request->get('length'));
            $parcel->setWidth($request->get('width'));
            $parcel->setHeight($request->get('height'));
            $parcel->setWeight($request->get('weight'));
            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($request->get('user_id'));
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $parcel->setUser($user);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($parcel);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcel);
            $em->flush();
            return $parcel;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }
}

