<?php
namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\User;
use App\Entity\Address;
use App\Entity\State;
use App\Entity\Shipment;
use App\Entity\Insurance;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class InsuranceController extends FOSRestController
{
    /**
     * @Rest\Get("/insurance")
     * @ ParamConverter("user", class="ApiBundle:User")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('ApiBundle:Insurance')->findAll();
        if ($restresult === null) {
            return new View("there are no insurance exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/insurance/{id}")
     * @ParamConverter("insurance", class="ApiBundle:Insurance")
     */
    public function idAction(Insurance $insurance)
    {
        if ($insurance === null) {
            return new View("insurance not found", Response::HTTP_NOT_FOUND);
        }
        return $insurance;
    }    

    /**
    * @Rest\Post("/insurance/")
    */
    public function postAction(Request $request)
    {
        try {
            $insurance = new Insurance;
            $insurance->setAmount($request->getAmount('amount'));
            $shipment = $this->getDoctrine()->getRepository('ApiBundle:Shipment')->find($request->get('shipment_id'));
            if (!$shipment) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $insurance->setShipment($shipment);
            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->find($request->get('user_id'));
            if (!$user) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $insurance->setUser($user);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($insurance);
            if (count($errors) > 0) {
                throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($insurance);
            $em->flush();
            return $insurance;
        } catch (\Exception $e) {
            throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException();
        }
    }
}

