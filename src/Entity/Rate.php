<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rate
 *
 * @ORM\Table(name="rate")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\RateRepository")
 */
class Rate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Courier")
     * @ORM\JoinColumn(name="courier_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $courier;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Shipment", inversedBy="rates")
     * @ORM\JoinColumn(name="shipment_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $shipment;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courier
     *
     * @param \stdClass $courier
     *
     * @return Rate
     */
    public function setCourier($courier)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return \stdClass
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Set shipping
     *
     * @param \stdClass $shipping
     *
     * @return Rate
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return \stdClass
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Rate
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Rate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

