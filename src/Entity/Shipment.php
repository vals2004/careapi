<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Shipment
 *
 * @ORM\Table(name="shipment")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ShipmentRepository")
 */
class Shipment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="from_address_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $fromAddress;

    /**
     * @var \ApiBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="to_address_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $toAddress;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @var \ApiBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var \ApiBundle\Entity\Parcel
     *
     * @ORM\ManyToOne(targetEntity="Parcel")
     * @ORM\JoinColumn(name="parcel_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $parcel;

    /**
     * @var \ApiBundle\Entity\Pickup
     *
     * @ORM\OneToOne(targetEntity="Pickup", mappedBy="shipment")
     */
    private $pickup;

    /**
     * @ORM\Column(name="status", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="shipment")
     */
    private $rates;

    /**
     * @ORM\Column(name="ups_response", type="text")
     */
    private $upsResponse;

    public function __construct()
    {
        $this->rates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get string representation
     *
     * @return string
     */
    public function __toString()
    {
        return 'Shipment #'.$this->id;
    }

    /**
     * Set fromAddress
     *
     * @param \ApiBundle\Entity\Address $fromAddress
     *
     * @return Shipment
     */
    public function setFromAddress(Address $fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * Get fromAddress
     *
     * @return \ApiBundle\Entity\Address
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * Set toAddress
     *
     * @param \ApiBundle\Entity\Address $toAddress
     *
     * @return Shipment
     */
    public function setToAddress(Address $toAddress)
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    /**
     * Get toAddress
     *
     * @return \ApiBundle\Entity\Address 
     */
    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Shipment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return Address
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set parcel
     *
     * @param \ApiBundle\Entity\Parcel $parcel
     *
     * @return Shipment
     */
    public function setPickup(Pickup $pickup)
    {
        $pickup->setShipment($this);
        $this->pickup = $pickup;

        return $this;
    }

    /**
     * Get parcel
     *
     * @return \ApiBundle\Entity\Parcel
     */
    public function getPickup()
    {
        return $this->pickup;

    }

    /**
     * Set parcel
     *
     * @param \ApiBundle\Entity\Parcel $parcel
     *
     * @return Shipment
     */
    public function setParcel(Parcel $parcel)
    {
        $this->parcel = $parcel;

        return $this;
    }

    /**
     * Get parcel
     *
     * @return \ApiBundle\Entity\Parcel
     */
    public function getParcel()
    {
        return $this->parcel;

    }

    public function getRates()
    {
        return $this->rates;
    }

    public function getUpsResponse()
    {
        return $this->upsResponse;
    }

    public function setUpsResponse($upsResponse)
    {
        $this->upsResponse = $upsResponse;

        return $this;
    }
}
